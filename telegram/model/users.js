var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var TableSchema = new Schema({
	telegram_id: {
        type: Number
    }
});

module.exports = mongoose.model("user", TableSchema);


// var TableSchema = new Schema({
// 	telegram_id: {type: Number, required: true},
// 	description: {type: String, required: true},
// 	isbn: {type: String, required: true},
// 	user: { type: Schema.ObjectId, ref: "User", required: true },
// }, {timestamps: true});

// module.exports = mongoose.model("user", TableSchema);